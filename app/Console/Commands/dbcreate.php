<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class dbcreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:create {name?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new MySQL database based on the database env file or the provided name.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $database = env('DB_DATABASE', false);
        $database = $this->argument('name') ?: $database;

        if (!$database) {
            $this->info('Skipping creation of database name is empty!');
            return;
        }

        try {
            config(["database.connections.mysql.database" => null]);

            $charset = env('DB_CHARSET') ?: "";
            $collation = env('DB_COLLATION') ?: "";
            $query = "CREATE DATABASE IF NOT EXISTS $database CHARACTER SET '$charset' COLLATE '$collation';";
            DB::statement($query);

            config(["database.connections.mysql.database" => $database]);
            $this->info(sprintf('Successfully created %s database', $database));
        } catch (Exception $exception) {
            $this->error(sprintf('Failed to create %s database, %s', $database, $exception->getMessage()));
        }
    }
}
